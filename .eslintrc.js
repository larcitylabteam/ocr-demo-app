module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      modules: true
    }
  },
  plugins: [
    'react',
    'react-native',
    'mocha',
    'jsx-a11y'
  ],
  env: {
    node: true,
    mocha: true
  },
  globals: {
    fetch: true
  },
  rules: {
    semi: ['warn', 'never'],
    'no-console': 0
  }
}