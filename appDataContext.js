import React from 'react'
//import firebase from 'firebase'

const cf = require('./private/fbse.json')

export default React.createContext({
  firebaseConfig: cf.config,
  credentials: cf.app_user,
})
