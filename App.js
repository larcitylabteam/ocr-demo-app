import { Permissions, } from 'expo'
import React from 'react'

import AppNavigator from './components/AppNavigator'

import AppDataContext from './appDataContext'
import DeviceContext from './deviceContext'
import firebase from 'firebase'

export default class Root extends React.PureComponent {

  displayName = 'RootController'

  state = {
    firebaseIsConfigured: false
  }

  componentWillMount = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION)
    console.log(status, `Returned from location permission request @${this.displayName}`)
    this.setState({ locationPermissionGranted: status === 'granted' })
  }

  render() {
    let { firebaseIsConfigured } = this.state
    const c = this
    return (
      <DeviceContext.Consumer>
        {deviceProps => {
          return (
            <AppDataContext.Consumer>
              {(sessProps) => {
                const { firebaseConfig, credentials } = sessProps
                if (!firebaseIsConfigured) {
                  // console.log('Firebase configuration -> ', firebaseConfig)
                  // initialize firebase
                  firebase.initializeApp(firebaseConfig)
                  c.setState({ firebaseIsConfigured: true })
                }
                // component
                return (
                  <AppNavigator
                    screenProps={{
                      firebaseConfig,
                      credentials,
                      device: deviceProps
                    }} />
                )
              }}
            </AppDataContext.Consumer>
          )
        }}
      </DeviceContext.Consumer>
    )
  }
}