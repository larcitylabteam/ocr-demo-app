// Built with guidance from: https://github.com/expo/camerja/blob/master/App.js
import { Permissions, Constants, Camera, FileSystem, ImageManipulator, /*ScreenOrientation,*/ } from 'expo'
import React from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet, View, Text, /*TouchableOpacity,*/ Slider, Vibration,
  ActivityIndicator,
  Modal,
} from 'react-native'
import Orientation from 'react-native-orientation'
import { Icon } from 'react-native-elements'
import isIPhoneX from 'react-native-is-iphonex'
import uuid from 'uuid'
import moment from 'moment-timezone'
import firebase from 'firebase'

const flashModeOrder = {
  off: 'on',
  on: 'auto',
  auto: 'torch',
  torch: 'off',
}

export default class CameraScreen extends React.Component {
  static propTypes = {
    screenProps: PropTypes.shape({
      device: PropTypes.shape({
        isPortrait: PropTypes.func.isRequired,
        isLandscape: PropTypes.func.isRequired
      })
    })
  }

  // Default state definition
  state = {
    appIsBusy: false,
    flash: 'off',
    permissionsGranted: false,
    autoFocus: 'on',
    depth: 0,
    type: 'back',
    whiteBalance: 'auto',
    zoom: 0,
    photoId: 1,
    photos: [],
    ratio: '16:9',
    ratios: [],
    faces: []
  }

  componentWillMount = async () => {
    // attempt to allow ONLY portrait orientation
    // ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT_UP)
    // @TODO look into orientation handling - this doesn't appear to be working
    try {
      Orientation.lockToPortrait()
      Orientation.addOrientationListener(this._orientationDidChange)
    } catch (err) {
      console.log(err, 'Problem locking orientation')
    }
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    return this.setState({ permissionsGranted: status === 'granted' })
  }

  componentDidMount = async () => {
    const PHOTO_DIR = FileSystem.documentDirectory + 'photos'
    const info = await FileSystem.getInfoAsync(PHOTO_DIR)
    // create directory if it does not exist
    if (!info.exists) {
      try {
        const createResponse = await FileSystem.makeDirectoryAsync(PHOTO_DIR)
        console.log(createResponse, 'Response to request for mkdir')
      } catch (err) {
        console.log(err, 'There was a problem making the photo directory')
      }
    }
    console.log(info, 'Photo Directory')
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      // do something
    } else {
      // do something with portrait layout
    }
  }

  getRatios = async () => {
    const ratios = await this.camera.getSupportedRatios()
    return ratios
  }

  // // @TODO see if there is a way to extract image location data from EXIF
  // saveImageData = async (imageData) => {
  //   const { exif, uri, width, height, } = imageData
  //   if (!exif) return null
  //   // found exif data 
  // }

  compressImageAndFixOrientation = async (uri, exif) => {
    //let { exif, uri } = data
    let transformActions = []
    let saveOptions = {
      compress: 0.75,
      base64: false,
      format: 'png',
    }
    let transformedImage = null
    // resize image
    transformActions.push({
      resize: {
        width: 480
      }
    })
    // apply orientation fixes
    if (exif) {
      // transform image orientation IF orientation problem is detected
      // switch (Number.parseInt(exif.Orientation)) {
      //   case -90:
      //     console.log('Rotating image 90 deg...')
      //     // image shows up as upside down - fix this
      //     transformActions.push({ rotate: exif.Orientation })
      //     break
      // }
    }
    transformedImage = await ImageManipulator.manipulate(uri, transformActions, saveOptions)
    // update image URI
    console.log(`Old image URI: ${uri}`)
    console.log(`New image URI: ${transformedImage.uri}`)
    return transformedImage
  }

  uploadFileAsync = async (URI) => {
    const response = await fetch(URI)
    const blob = await response.blob()
    // For formatting documentation: http://momentjs.com/docs/#/parsing/string-format/
    const NOW = moment().tz('America/New_York')
    console.log('Blob of image created successfully!')
    const ref = firebase.storage()
      .ref('receipts')
      .child(`${NOW.format('x')}-${uuid.v4()}`)
    const snapshot = await ref.put(blob)
    // return URL for uploaded image
    return snapshot.downloadURL
  }

  takePicture = async () => {
    const { navigation } = this.props
    const c = this
    let localImage = null
    if (this.camera) {
      const data = await this.camera.takePictureAsync({ exif: true })
      this.setState({
        appIsBusy: true,
        photoId: this.state.photoId + 1,
      }, async () => {
        let localImageURI = `${FileSystem.documentDirectory}photos/Photo_${this.state.photoId}.jpg`
        const { uri, width, height, exif, } = data
        console.log(`Image original dimensions: ${width}x${height}`)
        console.log(exif, 'EXIF image data')
        // save to local filesystem
        await FileSystem.moveAsync({ from: uri, to: localImageURI })
        // update local image variable
        localImage = {
          uri: localImageURI,
          width,
          height,
          originalExif: exif
        }
        // @TODO compression failing with NSTaggedPointerString error "unrecognized selector sent to instance"
        try {
          // @TODO handle error
          const compressedImage = await this.compressImageAndFixOrientation(localImage.uri, exif)
          //console.log('Compressed image -> ', compressedImage)
          localImage = Object.assign({ wasLocallyCompressed: true }, localImage, compressedImage)
          console.log('Final image -> ', localImage)
        } catch (err) {
          console.log(err, 'Image compression error')
        }
        //console.log('Capture result -> ', data)
        Vibration.vibrate()
        // save to remote storage
        try {
          const result = await c.uploadFileAsync(localImage.uri)
          console.log('Upload result -> ', result)
        } catch (err) {
          console.log('There was a problem uploading your image -> ', err)
        }
        this.setState({ appIsBusy: false })
        navigation.navigate('Result', { localImage })
      })
    }
  }

  toggleFlash() {
    this.setState({
      flash: flashModeOrder[this.state.flash],
    })
  }

  setRatio(ratio) {
    this.setState({
      ratio,
    })
  }

  zoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    })
  }

  zoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    })
  }

  setFocusDepth(depth) {
    this.setState({
      depth,
    })
  }

  renderNoPermissions() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
        <Text style={{ color: 'white' }}>
          Camera permissions not granted - cannot open camera preview.
        </Text>
      </View>
    )
  }

  renderCamera() {
    return (
      <Camera
        ref={ref => {
          this.camera = ref
        }}
        style={{
          flex: 1,
        }}
        type={this.state.type}
        flashMode={this.state.flash}
        autoFocus={this.state.autoFocus}
        zoom={this.state.zoom}
        whiteBalance={this.state.whiteBalance}
        ratio={this.state.ratio}
        focusDepth={this.state.depth}>
        <View
          style={{
            flex: 0.5,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            justifyContent: 'space-around',
            paddingTop: Constants.statusBarHeight / 2,
          }}>
          {/* 
          <TouchableOpacity style={styles.flipButton} onPress={this.toggleFacing.bind(this)}>
            <Text style={styles.flipText}> FLIP </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.flipButton} onPress={this.toggleFlash.bind(this)}>
            <Text style={styles.flipText}> FLASH: {this.state.flash} </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.flipButton} onPress={this.toggleWB.bind(this)}>
            <Text style={styles.flipText}> WB: {this.state.whiteBalance} </Text>
          </TouchableOpacity>
          */}
        </View>
        <View
          style={{
            flex: 0.4,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignSelf: 'flex-end',
            marginBottom: -5,
          }}>
          {this.state.autoFocus !== 'on' ? (
            <Slider
              style={{ width: 150, marginTop: 15, marginRight: 15, alignSelf: 'flex-end' }}
              onValueChange={this.setFocusDepth.bind(this)}
              step={0.1}
            />
          ) : null}
        </View>
        <View
          style={{
            flex: 0.1,
            paddingBottom: isIPhoneX ? 20 : 0,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            //alignSelf: 'flex-end',
            alignSelf: "center"
          }}>
          {/* 
          <TouchableOpacity
            style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
            onPress={this.zoomIn.bind(this)}>
            <Text style={styles.flipText}> + </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
            onPress={this.zoomOut.bind(this)}>
            <Text style={styles.flipText}> - </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.flipButton, { flex: 0.25, alignSelf: 'flex-end' }]}
            onPress={this.toggleFocus.bind(this)}>
            <Text style={styles.flipText}> AF : {this.state.autoFocus} </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.flipButton, styles.picButton, { flex: 0.3, alignSelf: 'flex-end' }]}
            onPress={this.takePicture.bind(this)}>
            <Text style={styles.flipText}> SNAP </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.flipButton, styles.galleryButton, { flex: 0.25, alignSelf: 'flex-end' }]}
            onPress={this.toggleView.bind(this)}>
            <Text style={styles.flipText}> Gallery </Text>
          </TouchableOpacity>
          */}
          <Icon raised name={'camera'}
            type={'material-community-icons'}
            size={40}
            onPress={this.takePicture.bind(this)}
          />
        </View>
        {
          /* 
          {this.renderFaces()}
          {this.renderLandmarks()}
          */
        }
      </Camera>
    )
  }

  // componentWillReceiveProps = async (nextProps) => {
  //   // check orientation 
  //   const { screenProps } = nextProps
  //   const { device } = screenProps
  //   console.log('isPortrait? -> ', device.isPortrait())
  // }

  render() {
    const { appIsBusy } = this.state
    const cameraScreenContent = this.state.permissionsGranted ? this.renderCamera() : this.renderNoPermissions()
    return (
      <View style={styles.cameraContainer}>
        {cameraScreenContent}
        <Modal
          animationType={'fade'}
          transparent={true}
          visible={appIsBusy}
          presentationStyle={'overFullScreen'}>
          <View
            style={[styles.activityModalContainer, { alignItems: 'center' }]}
            backgroundColor={'rgba(255, 255, 255, 0.75)'}>
            <Text color={'#f7f7f7'}>Working...</Text>
            <ActivityIndicator size="large" animating={appIsBusy} hidesWhenStopped={true} />
          </View>
        </Modal>
      </View>
    )
  }

}
const styles = StyleSheet.create(require('../styles'))