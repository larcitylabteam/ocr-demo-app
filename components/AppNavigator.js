import { createStackNavigator } from 'react-navigation'
import React from 'react'
import { Header } from 'react-native-elements'

// components
import StartScreen from './Start'
import ResultScreen from './Result'
import CameraScreen from './Camera'

// root stack configuration
export const rootConfig = {
  initialRouteName: 'Start'
}

export default createStackNavigator({
  Result: {
    screen: ResultScreen,
    navigationOptions: ({ navigation }) => {
      return {
        header: (
          <Header
            outerContainerStyles={{ backgroundColor: 'yellow' }}
            leftComponent={{
              icon: 'home', type: 'font-awesome', text: 'Home',
              onPress: () => {
                navigation.navigate('Start')
              }
            }}
            rightComponent={{
              icon: 'camera', type: 'font-awesome', text: 'Scan Receipt',
              onPress: () => {
                navigation.navigate('Camera')
              }
            }}
            centerComponent={{ text: 'Capture Result' }} />
        ),
      }
    }
  },
  Start: {
    screen: StartScreen,
    navigationOptions: ({ navigation }) => {
      console.log(navigation.state.params)
      return {
        title: "Welcome",
        headerTitleAllowFontScaling: true,
        header: (
          <Header
            outerContainerStyles={{ backgroundColor: 'yellow' }}
            rightComponent={{
              icon: 'camera', type: 'font-awesome', text: 'Scan Receipt',
              onPress: () => {
                navigation.navigate('Camera')
              }
            }}
            centerComponent={{ text: 'Welcome' }} />
        ),
        // headerRight: (<CaptureButton />),
        // headerRight: (
        //   <Button
        //     iconRight={{
        //       name: 'camera',
        //       type: 'font-awesome',
        //       color: '#2a2a2a'
        //     }}
        //     color={'#2a2a2a'}
        //     title={'Receipt'}
        //     buttonStyle={{ backgroundColor: 'transparent' }}
        //     onPress={() => {
        //       // do when pressed
        //       navigation.navigate('Camera')
        //     }} />
        // ),
        headerTintColor: '#2a2a2a',
        headerStyle: {
          backgroundColor: 'yellow'
        }
      }
    }
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: ({ navigation }) => {
      console.log(navigation.state.params)
      return {
        // header: (
        //   <Header
        //     outerContainerStyles={{ backgroundColor: 'yellow' }}
        //     centerComponent={{
        //       title: "Capture Receipt"
        //     }} />
        // ),
        headerStyle: { backgroundColor: 'yellow' },
        title: "Capture Receipt",
        headerTintColor: '#2a2a2a'
      }
    }
  }
}, rootConfig)
