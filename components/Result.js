import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import AppDataContext from '../appDataContext'

export default class Result extends React.Component {

  static propTypes = {
    navigation: PropTypes.shape({
      state: PropTypes.shape({
        params: PropTypes.object
      })
    })
  }

  displayName = 'ResultComponent'

  render() {
    const { params } = this.props.navigation.state
    console.log(params, `Parameters @${this.displayName}`)
    const { localImage } = params

    return (
      <AppDataContext.Consumer>
        {sess => {
          console.log(`Sess @${this.displayName} -> `, sess)
          return (
            <View style={styles.container}>
              {localImage ? (
                <Image
                  style={styles.picture}
                  source={{
                    uri: localImage.uri
                  }} />
              ) : <Text>No Image</Text>}
            </View>
          )
        }}
      </AppDataContext.Consumer>
    )
  }
}

const styles = StyleSheet.create(require('../styles'))