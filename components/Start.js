import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet, View, Text, } from 'react-native'
import firebase from 'firebase'

export default class StartScreen extends React.Component {

  static propTypes = {
    screenProps: PropTypes.shape({
      credentials: PropTypes.object
    })
  }

  displayName = 'StartScreenComponent'

  componentWilMount = async () => { }

  componentDidMount() {
    const { screenProps } = this.props
    const { credentials } = screenProps
    console.log(`Props  -> @${this.displayName} -> `, screenProps)
    // listen for login 
    firebase.auth().onAuthStateChanged(user => {
      console.log('Authenticated user -> ', user)
    })
    // @TODO @IMPORTANT - only do this if no credentials are available
    const { username, password } = credentials
    // login
    firebase.auth().signInWithEmailAndPassword(username, password)
      .catch(err => {
        throw err
        // do something if error occurs
      })
  }

  render() {
    return (
      <View style={[styles.container, { padding: 20 }]}>
        <Text style={{ fontSize: 34, fontWeight: "500" }}>What is this?</Text>
        <View style={{ marginTop: 20 }}>
          <Text style={{ fontSize: 22 }}>A demo application that captures a receipt image, and sends the file data onwards to a GCP function for OCR processing.</Text>
        </View>
        <View style={{ marginTop: 10 }}>
          <Text style={{ fontSize: 22 }}>For notes on how I built this app, visit:</Text>
          <Text style={{ fontSize: 22, fontWeight: "600" }}>http://bit.ly/2JW4gMq</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create(require('../styles'))