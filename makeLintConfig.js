const config = require('./.eslintrc')
const fs = require('fs')
const path = require('path')
// write JSON config file
fs.writeFileSync(path.join(process.cwd(), '.eslintrc.json'), new Buffer(JSON.stringify(config, null, '\t'), 'utf8'))