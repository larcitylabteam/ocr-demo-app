import React from 'react'
//import firebase from 'firebase'
import { Dimensions } from 'react-native'

export default React.createContext({
  // helper functions
  msp: (dim, limit) => {
    return (dim.scale * dim.width) >= limit || (dim.scale * dim.height) >= limit
  },
  isPortrait: () => {
    const dim = Dimensions.get('screen')
    return dim.height >= dim.width
  },
  isLandscape: () => {
    const dim = Dimensions.get('screen')
    return dim.width >= dim.height
  },
  isTablet: () => {
    const dim = Dimensions.get('screen')
    return ((dim.scale < 2 && this.msp(dim, 1000)) || (dim.scale >= 2 && this.msp(dim, 1900)))
  },
  isPhone: () => { return !this.isTablet() }
})
